﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{

    public class OperarViewModel
    {
        public int Cuenta { get; set; }
        public int IdCliente { get; set; }
        public int TipoOperacion { get; set; }

        public int Saldo { get; set; }

        public int ValorTransaccion { get; set; }
    }
    public class Cuenta
    {
        public int Id { get; set; }
        public int numero_cuenta { get; set; }
        public int saldo { get; set; }
    }
    public class IndexController : Controller
    {
        // datos de prueba
         Cuenta[] cuentas = new Cuenta[] {
                new Cuenta{ Id = 1, numero_cuenta = 12323232, saldo = 123123 },
                new Cuenta{ Id = 2, numero_cuenta = 345512 , saldo = 1231231},
                new Cuenta{ Id = 3, numero_cuenta = 12223233, saldo = 223123 }
            };
        
        public JsonResult SaldoCuenta(int id_cuenta)
        {
            return Json(cuentas.First(c => c.Id == id_cuenta).saldo,JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Detalle(int id)
        {
            OperarViewModel o = new OperarViewModel
            {
                IdCliente = id
            };

            var tipoOperacion = new object[] {
                new { Id = 1, nombre = "tipo operacion 1" },
                new { Id = 2, nombre = "tipo operacion 2" },
                new { Id = 3, nombre = "tipo operacion 3" }
            };
            ViewBag.Cuenta = new SelectList(cuentas,"Id","numero_cuenta");
            ViewBag.TipoOperacion = new SelectList(tipoOperacion, "Id", "nombre");
            return View(o);
        }
        [HttpPost]
        public ActionResult Detalle(OperarViewModel model)
        {

            //aqui llegan los datos dentro de model




            return View();
        }

        
    }
}
